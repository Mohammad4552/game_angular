import { Component, Input, OnInit } from '@angular/core';
import { CellEnum } from './CellEnum';

@Component({
  selector: 'app-cell',
  templateUrl: './cell.component.html',
  styleUrls: ['./cell.component.css']
})
export class CellComponent implements OnInit {

  @Input() public piece: CellEnum= CellEnum.Empty;
  @Input() public row: number=0;
  @Input() public col: number=0;

  ngOnInit(): void {
  }

}
